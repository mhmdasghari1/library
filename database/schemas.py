from pydantic import BaseModel
from typing import Optional


class BookBase(BaseModel):
    name: str
    author: Optional[str]


class Book(BookBase):
    id: int
    reversed: bool
    borrowed: bool
    user_id: int
    time: str

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    username: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int

    class Config:
        orm_mode = True


class Token(BaseModel):
    access_token: str


class TokenData(BaseModel):
    user_id: Optional[str] = None
