from elasticsearch import Elasticsearch
from sqlalchemy.orm import Session
from database import crud

es = Elasticsearch(hosts="localhost", port="9200")


def save_on_elastic(db: Session):
    books = crud.get_all_books(db)
    if books:
        i = 1
        for book in books:
            b = {
                "id": f"{book.id}",
                "name": f"{book.name}",
                "author": f"{book.reserved}",
                "borrowed": f"{book.borrowed}",
                "time": f"{book.time}",
                "user_id": f"{book.user_id}"
            }
            es.index(index='books', body=b, id=i)
            i += 1
    users = crud.get_all_users(db)
    if users:
        i = 1
        for user in users:
            u = {
                "id": f"{user.id}",
                "username": f"{user.username}",
                "hashed_password": f"{user.hashed_password}",
                "penalty": f"{user.penalty}"
            }
            es.index(index='users', body=u, id=i)
            i += 1
    admin = crud.get_admin(db)
    if admin:
        i = 1
        a = {
            "id": f"{admin.id}",
            "username": f"{admin.username}"
        }
        es.index(index='users', body=a, id=i)
