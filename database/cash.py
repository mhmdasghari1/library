import redis

redis_db = redis.Redis(host='localhost', port=6379, db=0)


def get_token(user_id: str):
    return redis_db.get(user_id)


def set_token(user_id: int, token: str):
    return redis_db.set(str(user_id), token)
