from sqlalchemy.orm import Session
from datetime import datetime
from database import (schemas, models)


def get_user(db: Session, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def get_user_by_id(db: Session, user_id: str):
    return db.query(models.User).filter(models.User.id == int(user_id)).first()


def create_user(db: Session, user: schemas.UserCreate):
    db_user = models.User(username=user.username, hashed_password=user.password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)


def get_all_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.User).offset(skip).limit(limit).all()


def get_admin(db: Session):
    return db.query(models.Admin).first()


def is_admin(db: Session, username: str):
    return db.query(models.Admin).filter(models.Admin.username == username).first()


def is_there_admin(db: Session):
    return db.query(models.Admin).first()


def create_admin(db: Session, user: schemas.UserBase):
    db_admin = models.Admin(username=user.username)
    db.add(db_admin)
    db.commit()
    db.refresh(db_admin)


def get_book(db: Session, name: str):
    return db.query(models.Book).filter(models.Book.name == name).first()


def create_book(db: Session, book: schemas.BookBase):
    db_book = models.Book(name=book.name, author=book.author)
    db.add(db_book)
    db.commit()
    db.refresh(db_book)


def get_all_books(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Book).offset(skip).limit(limit).all()


def reserve_book(db: Session, book: schemas.Book, user: schemas.User):
    b = db.query(models.Book).filter(models.Book.name == book.name).first()
    b.reserved = True
    b.user_id = user.id
    b.time = datetime.now().strftime("%y-%m-%d %H:%M:%S")
    db.commit()
    db.refresh(b)


def borrow_book(db: Session, book: schemas.Book, user: schemas.User):
    b = db.query(models.Book).filter(models.Book.name == book.name).first()
    b.borrowed = True
    b.user_id = user.id
    b.time = datetime.now().strftime("%y-%m-%d %H:%M:%S")
    db.commit()
    db.refresh(b)


def unreserve_book(db: Session, book: schemas.Book):
    b = db.query(models.Book).filter(models.Book.name == book.name).first()
    b.reserved = False
    b.user_id = None
    b.time = None
    db.commit()
    db.refresh(b)


def unborrowed_book(db: Session, book: schemas.Book):
    b = db.query(models.Book).filter(models.Book.name == book.name).first()
    b.borrowed = False
    user_id = b.user_id
    b.user_id = None
    b.time = None
    db.commit()
    db.refresh(b)
    return user_id


def penalty(db: Session, user_id: int):
    user = db.query(models.User).filter(models.User.id == user_id).first()
    user.penalty += 1
    db.commit()
    db.refresh(user)
