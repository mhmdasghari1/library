from sqlalchemy import (Column, String, Integer, Boolean, ForeignKey)
from database.database import Base


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, unique=True, primary_key=True, index=True, nullable=False)
    username = Column(String, unique=True, primary_key=False, nullable=False)
    hashed_password = Column(String, primary_key=False, nullable=False)
    penalty = Column(Integer, nullable=True)


class Book(Base):
    __tablename__ = "book"
    id = Column(Integer, unique=True, primary_key=True, index=True, nullable=False)
    name = Column(String, unique=True, nullable=False)
    author = Column(String, nullable=False)
    reserved = Column(Boolean, default=False)
    borrowed = Column(Boolean, default=False)
    time = Column(String, nullable=True)
    user_id = Column(Integer, ForeignKey('user.id'))


class Admin(Base):
    __tablename__ = "admin"
    id = Column(Integer, unique=True, primary_key=True, index=True, nullable=False)
    username = Column(String, nullable=False, unique=True)
