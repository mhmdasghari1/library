from database.database import (SessionLocal, engine)
from database import (database, elastic, cash, crud, schemas, models)
from fastapi.security import (OAuth2PasswordBearer, OAuth2PasswordRequestForm)
from fastapi.responses import JSONResponse
from fastapi import (Depends, FastAPI, HTTPException)
from sqlalchemy.orm import Session
from datetime import datetime
import jwt1
import password

app = FastAPI()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
database.Base.metadata.create_all(bind=engine)


def get_db():
    db = SessionLocal()
    try:
        return db
    finally:
        db.close()


def check_books(db: Session = Depends(get_db)):
    books = crud.get_all_books(db)
    for book in books:
        if book.borrowed or book.reserved:
            delta = datetime.strptime(book.time,
                                      "%y-%m-%d %H:%M:%S") - datetime.now()
            if delta.days > 2:
                crud.unreserve_book(db, book)
            if delta.days > 14:
                user_id = crud.unborrowed_book(db, book)
                crud.penalty(db, user_id)
    elastic.save_on_elastic(db)


def get_current_user(db: Session = Depends(get_db),
                     token: str = Depends(oauth2_scheme)):
    check_books(db)
    return jwt1.decoded_access_token(db, token)


def is_admin_logged(db: Session = Depends(get_db)):
    return crud.get_admin(db)


@app.post("/token/", response_model=schemas.Token)
def login_for_access_token(db: Session = Depends(get_db),
                           form_data: OAuth2PasswordRequestForm = Depends()):
    user = jwt1.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=401,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    token = jwt1.create_access_token(user_id=user.id)
    header = {"Authorization": f"Bearer {token}"}
    cash.set_token(user.id, token)
    return JSONResponse(headers=header)


@app.post("/signup/", response_model=schemas.User)
def signup(user_data: schemas.UserCreate, db: Session = Depends(get_db)):
    user = crud.get_user(db, user_data.username)
    if user:
        raise HTTPException(status_code=409,
                            detail="User already registered.")
    user_data.password = password.get_password_hash(user_data.password)
    signed_up_user = crud.create_user(db, user_data)
    return signed_up_user


@app.get("/me/", response_model=schemas.User)
def read_logged_in_user(current_user: models.User = Depends(get_current_user)):
    return current_user


@app.post("/admin/")
def make_a_user_admin(user_data: schemas.UserBase, db: Session = Depends(get_db)):
    admin = crud.is_there_admin(db)
    if admin:
        raise HTTPException(status_code=401,
                            detail=f"{crud.get_admin(db).username} is admin. you cant add anymore.")
    admin = crud.create_admin(db, user_data)
    return admin


@app.post("/add-book/", response_model=schemas.BookBase)
def add_book(book_data: schemas.BookBase,
             db: Session = Depends(get_db),
             admin: schemas.UserBase = Depends(is_admin_logged),
             current_user: models.User = Depends(get_current_user)):
    if admin:
        if admin.username == current_user.username:
            book = crud.get_book(db, book_data.name)
            if book:
                raise HTTPException(status_code=401,
                                    detail="You can't add more than one")
            book_going_to_add = crud.create_book(db, book_data)
            return book_going_to_add
        else:
            raise HTTPException(status_code=403,
                                detail="Admin is not logged in")
    else:
        raise HTTPException(status_code=403,
                            detail="Only admin can add books.")


@app.get("/get-all-books")
def get_all_books(db: Session = Depends(get_db)):
    books = crud.get_all_books(db)
    return books


@app.post("/reserve/")
def reserve(book_name: schemas.BookBase,
            db: Session = Depends(get_db),
            current_user: models.User = Depends(get_current_user)):
    book = crud.get_book(db, book_name.name)
    if book:
        if book.reserved:
            return f"Book {book.name} was reserved. You can't reserve it."
        if book.borrowed:
            return f"Book {book.name} was borrowed. You can't reserve it."
        crud.reserve_book(db, book, current_user)
        return "Reserved!"
    else:
        raise HTTPException(status_code=404,
                            detail="Book not found")


@app.post("/borrow/")
def borrow(book_name: schemas.BookBase,
           db: Session = Depends(get_db),
           current_user: models.User = Depends(get_current_user)):
    book = crud.get_book(db, book_name.name)
    if book:
        if book.reserved:
            return f"Book {book.name} was reserved. You can't borrow it."
        if book.borrowed:
            return f"Book {book.name} was borrowed. You can't borrow it."
        crud.borrow_book(db, book, current_user)
        return "Borrowed!"
    else:
        raise HTTPException(status_code=404,
                            detail="Book not found")
